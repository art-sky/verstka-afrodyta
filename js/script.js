$(document).ready(function(){
	$("#menu").on("click","a", function (event) {
		event.preventDefault();

		var id  = $(this).attr('href'),

			top = $(id).offset().top;

		$('body,html').animate({scrollTop: top}, 500);
	});

	$("#banner").on("click","a", function (event) {
		event.preventDefault();

		var id  = $(this).attr('href'),

			top = $(id).offset().top;

		$('body,html').animate({scrollTop: top}, 1000);
	});

	$("header").removeClass("default");
	$(window).scroll(function(){
		if ($(this).scrollTop() > 1) {
			$("header").addClass("default").fadeIn('fast');
		} else {
			$("header").removeClass("default").fadeIn('fast');
		};
	});

	$('body').append('<button class="btn_up" />');

	$('.btn_up').click(function(){
		$('body').animate({'scrollTop': 0}, 500);
		$('html').animate({'scrollTop': 0}, 500);
	});

	$(window).scroll(function(){
		if($(window).scrollTop() > 400){
			$('.btn_up').addClass('active');
		}
		else{
			$('.btn_up').removeClass('active');
		}
	});
});

