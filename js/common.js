$(function() {
	$("#banner h1").animated("fadeIn");
	$("section p").animated("fadeInUp");
	$("section a").animated("pulse");
	$("#art1 h1").animated("fadeInLeft");
	$("#art1 h3").animated("fadeInRight");
	$("#art1 img").animated("fadeInRight");
	$("#art2 h1").animated("fadeInRight");
	$("#art2 h2").animated("fadeInDown");
	$("#art2 h3").animated("fadeInLeft");
	$("#art2 img").animated("fadeInLeft");
	$("#art3 h1").animated("fadeInLeft");
	$("#art3 h3").animated("fadeInRight");
	$("#art3 img").animated("fadeInRight");
	$("#news h1").animated("fadeIn");
	$("#categories figure").animated("fadeIn");
	$(".menu").animated("fadeIn");
	$(".menu22").animated("fadeIn");
	$(".menu33").animated("fadeIn");
	$("#art4 h1").animated("fadeInRight");
	$("#art4 h2").animated("fadeInDown");
	$("#art4 h3").animated("fadeInLeft");
	$("#art4 img").animated("fadeInLeft");
	$(".soc").animated("fadeInUp");
	$(".p-text").animated("fadeInUp");
	$("header img").animated("fadeIn");
	
	


	//SVG Fallback
	if(!Modernizr.svg) {
		$("img[src*='svg']").attr("src", function() {
			return $(this).attr("src").replace(".svg", ".png");
		});
	};

	//E-mail Ajax Send
	//Documentation & Example: https://github.com/agragregra/uniMail
	$("form").submit(function() { //Change
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "mail.php", //Change
			data: th.serialize()
		}).done(function() {
			alert("Thank you!");
			setTimeout(function() {
				// Done Functions
				th.trigger("reset");
			}, 1000);
		});
		return false;
	});

	//Chrome Smooth Scroll
	try {
		$.browserSelector();
		if($("html").hasClass("chrome")) {
			$.smoothScroll();
		}
	} catch(err) {

	};

	$("img, a").on("dragstart", function(event) { event.preventDefault(); });
	
});

$(window).load(function() {

	$(".loader_inner").fadeOut();
	$(".loader").delay(400).fadeOut("slow");

});
